using System;
System.Collections
System.Collections.Generic


public class SudokuBoard
    {
        private readonly char[,] board;
     
        // validate board is a 9x9 array
        public SudokuBoard(char[,] board)
        {
            if (board == null || board.GetLength(0) != 9 || board.GetLength(1) != 9)
            {
               // Not a valid board
           }
    
           this.board = board;
       }
    
       public bool Validate()
       {
           
           
           var rows = Enumerable.Range(1, 9).Select(i => new HashSet<char>()).ToArray();
           var columns = Enumerable.Range(1, 9).Select(i => new HashSet<char>()).ToArray();
           var cubes = Enumerable.Range(1, 9).Select(i => new HashSet<char>()).ToArray();
    
           // process each cell only once
           for (int row = 0; row < 9; ++row)
           {
               for (int column = 0; column < 9; ++column)
               {
                   var current = board[row, column];
                   //Indicates whether the specified Unicode character is categorized as a decimal digit.
                   if (char.IsDigit(current))
                   {
                       // determine which of the "cubes" the row/col fall in
                       var cube = 3 * (row / 3) + (column / 3);
    
                       // if add to any set returns false, it was already there.
                       if (!rows[row].Add(current) || !columns[column].Add(current) || !cubes[cube].Add(current))
                       {
                           return false;
                       }
                   }
               }
           }
    
           return true;
       }
   }